export default () => {

    document.addEventListener('click', (event) => {
        const tooltip = document.querySelectorAll('[data-tooltip]');
        if (tooltip.length > 0) {
            if (event.target.closest('[data-tooltip-control]')) {
                let tooltipContainer = event.target.closest('[data-tooltip]');
                if (tooltipContainer.classList.contains('open')) {
                    tooltipContainer.classList.remove('open');
                }
                else {
                    tooltipClose();
                    tooltipContainer.classList.add('open');
                }
            }
            else {
                if (!event.target.closest('[data-tooltip-content]')) {
                    tooltipClose();
                }
            }
            function tooltipClose() {
                tooltip.forEach(el => {
                    el.classList.remove('open');
                });
            }
        }
    });
};
