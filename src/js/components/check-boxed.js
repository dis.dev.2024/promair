export default () => {
    window.addEventListener('load', function (e) {
        if(document.querySelectorAll('[data-check-group]').length > 0) {
            document.querySelectorAll('[data-check-group]').forEach(elem => {
                const checkAll = elem.querySelector('[data-check-all]')
                checkAll.addEventListener('change',  (event) => {
                    const checkboxItems = elem.querySelectorAll('[data-check-item]')

                    if (checkAll.checked) {
                        checkboxItems.forEach(el => {
                            el.checked = true
                            elem.closest('[data-check-group]').classList.add('group-checked')
                        })
                    }
                    else {
                        checkboxItems.forEach(el => {
                            el.checked = false
                            el.closest('[data-check-group]').classList.remove('group-checked')
                        })
                    }
                })
            });

            document.querySelectorAll('[data-check-item]').forEach(elem => {
                elem.addEventListener('change', (event) => {
                    const checkboxItems = elem.closest('[data-check-group]').querySelectorAll('[data-check-item]')
                    const checkboxAll = elem.closest('[data-check-group]').querySelector('[data-check-all]')
                    let counter = 0
                    checkboxItems.forEach(el => {
                        if (el.checked) {
                            counter = counter + 1
                        }
                    })
                    if (counter > 0) {
                        if (counter < checkboxItems.length) {
                            checkboxAll.indeterminate = true
                        }
                        else {
                            checkboxAll.indeterminate = false
                            checkboxAll.checked = true
                        }
                        elem.closest('[data-check-group]').classList.add('group-checked')
                    }
                    else {
                        checkboxAll.checked = false
                        checkboxAll.indeterminate = false
                        elem.closest('[data-check-group]').classList.remove('group-checked')
                    }
                })
            })
        }
    });
};
