export default () => {

    window.addEventListener("load", function (e) {

        if (document.querySelectorAll('[data-cart]').length > 0) {

            document.addEventListener('click',  (event) => {

                if(event.target.closest('[data-cart-add]')) {
                    event.preventDefault()
                    event.target.closest('[data-cart]').classList.add('added')
                    event.target.closest('[data-cart]').querySelector('[data-cart-quantity]').value = 1
                }

                if(event.target.closest('[data-cart-change]')) {
                    event.preventDefault()
                    const itemCart = event.target.closest('[data-cart]')
                    const itemCartInput = itemCart.querySelector('[data-cart-quantity]')
                    const itemCartMin = itemCartInput.min
                    const itemCartMax = itemCartInput.max
                    const itemCartDirection = event.target.closest('[data-cart-change]').dataset.cartChange
                    let itemCartValue = 0;

                    if(itemCartDirection === 'minus') {
                        itemCartValue = parseInt(itemCartInput.value) - 1
                    }
                    else {
                        itemCartValue = parseInt(itemCartInput.value) + 1
                    }
                    if (itemCartValue > 0) {
                        itemCart.classList.add('added')
                    }
                    else {
                        itemCartValue = 0
                        itemCart.classList.remove('added')
                    }
                    itemCartInput.value = itemCartValue
                }
            })

            document.querySelectorAll('[data-cart]').forEach(elem => {

                if (elem.querySelector('[data-cart-quantity]').value > 0) {
                    elem.classList.add('added')
                }

                elem.querySelector('[data-cart-quantity]').addEventListener('change', (el) => {
                    let item = el.target.closest('[data-cart]')

                    let val = parseInt(el.target.value)

                    if (val > 0) {
                        el.target.value = val
                        item.classList.add('added')
                    }
                    else {
                        el.target.value = 0
                        item.classList.remove('added')
                    }
                })
            });
        }
    });
};
