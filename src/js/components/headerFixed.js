export default function() {
    window.addEventListener('scroll', function() {

        if(document.querySelector('.header')) {
            let header = document.querySelector('.header-label');
            if (document.documentElement.scrollTop > header.offsetTop) {
                document.body.classList.add('fixed-header');
            } else {
                document.body.classList.remove('fixed-header');
            }
        }
    });

    window.addEventListener('scroll', function() {
        if(document.querySelector('.cart__float')) {
            let floatButton = document.querySelector('.cart__float');
            let baseButton = document.querySelector('.cart-order__button');

            let baseButtonOffset = parseInt(floatButton.offsetTop) + 28

            console.log('-----')
            console.log( floatButton.offsetTop)
            console.log( baseButton.getBoundingClientRect().top)
            console.log('-----')

            if (baseButton.getBoundingClientRect().top < baseButtonOffset) {
                floatButton.classList.add('cart-float-hide');
            } else {
                floatButton.classList.remove('cart-float-hide');
            }
        }
    });
}
