export default () => {
    if (document.querySelectorAll('[data-switcher]').length > 0) {
        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-switcher-button]')) {
                let selectContainer = event.target.closest('[data-switcher]');
                if (selectContainer.classList.contains('open')) {
                    selectContainer.classList.remove('open');
                }
                else {
                    switcherClose();
                    selectContainer.classList.add('open');
                }
            }
            else {
                if (!event.target.closest('[data-switcher-content]')) {
                    switcherClose();
                }
            }

            function switcherClose() {
                document.querySelectorAll('[data-switcher]').forEach(el => {
                    el.classList.remove('open');
                });
            }
        })

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-switcher-item]')) {
                let itemValue = event.target.closest('[data-switcher-item]').innerHTML
                event.target.closest('[data-switcher]').querySelectorAll('[data-switcher-item]').forEach(elem => {
                    elem.classList.remove('active');
                });
                event.target.closest('[data-switcher-item]').classList.add('active')
                event.target.closest('[data-switcher]').classList.remove('open')
                event.target.closest('[data-switcher]').querySelector('[data-switcher-active]').innerHTML = itemValue
            }
        })
    }
};
