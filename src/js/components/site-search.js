export default () => {
    window.addEventListener("load", function (e) {
        if (document.querySelectorAll('[data-search]').length > 0) {
            const searchItems = document.querySelectorAll('[data-search]');

            searchItems.forEach(el => {
                el.querySelector('[data-search-input]').addEventListener('keyup', (event) => {
                    const searchBlock = event.currentTarget.closest('[data-search]');
                    let searchValue = event.currentTarget.value;

                    searchItems.forEach(elem => {
                        elem.querySelector('[data-search-input]').value = searchValue

                        if (searchValue.length > 2) {
                            elem.classList.add('filled');
                            console.log(searchValue);
                        }
                        else {
                            elem.classList.remove('filled');
                        }
                    });
                })

                el.querySelector('[data-search-reset]').addEventListener('click', (event) => {
                    if (event.target.closest('[data-search-reset]')) {

                        searchItems.forEach(elem => {
                            elem.querySelector('[data-search-input]').value = ''
                            elem.classList.remove('filled');
                        });
                    }
                })
            });


            document.addEventListener('click', (event) => {
                if (!event.target.closest('[data-search]')) {
                    searchItems.forEach(el => {
                        el.classList.remove('filled');
                        el.querySelector('[data-search-input]').value = ''
                    })
                }
            })
        }
    });
};
