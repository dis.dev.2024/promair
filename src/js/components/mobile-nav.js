export default () => {
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            document.querySelector('body').classList.toggle('nav-open')
            document.querySelector('[data-nav-mobile]').classList.remove('nav-catalog-open')
            document.querySelectorAll('[data-nav-child]').forEach(elem => {
                elem.classList.remove('active');
            });
            return false
        }

        if(event.target.closest('[ data-nav-back]')) {
            event.target.closest('[data-nav-child]').classList.remove('active')
        }

        if(event.target.closest('[data-nav-parent]')) {
            const navPath = event.target.closest('[data-nav-parent]').dataset.navParent;

            document.querySelectorAll('[data-nav-child]').forEach(elem => {
                elem.classList.remove('active');
            });
            document.querySelector(`[data-nav-child="${navPath}"]`).classList.add('active');

            return false
        }
    })
};

