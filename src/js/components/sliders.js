/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/
import Swiper from 'swiper';
import { Navigation, Thumbs, Pagination, Autoplay } from 'swiper/modules';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров

    if (document.querySelector('[data-intro-primary]')) {
        new Swiper('[data-intro-primary]', {
            modules: [Navigation, Pagination, Autoplay],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 500,
            pagination: {
                el: '[data-intro-primary-pagination]',
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '[data-intro-primary-next]',
                prevEl: '[data-intro-primary-prev]',
            },
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            on: {}
        });
    }

    if (document.querySelector('[data-intro-secondary]')) {
        new Swiper('[data-intro-secondary]', {
            modules: [Navigation, Pagination, Autoplay],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 500,
            pagination: {
                el: '[data-intro-secondary-pagination]',
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '[data-intro-secondary-next]',
                prevEl: '[data-intro-secondary-prev]',
            },
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            on: {}
        });
    }

    if (document.querySelector('[data-comments]')) {
        new Swiper('[data-comments]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 6,
            speed: 500,
            breakpoints: {
                1024: {
                    slidesPerView: 4,
                },
            },
        });
    }

    if (document.querySelector('[data-partners]')) {
        new Swiper('[data-partners]', {
            modules: [Autoplay],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 24,
            speed: 500,
            breakpoints: {
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
            },
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            on: {
                init: (event) => {
                    if (event.slides.length > 2) {
                        console.log(event.hostEl)
                        event.hostEl.classList.add('shadow-left')
                    }
                },
            }
        });
    }

    if (document.querySelector('[data-news]')) {
        new Swiper('[data-news]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            speed: 500,
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 28,
                },
            },
            on: {
            }
        });
    }

    if (document.querySelector('[data-certificates]')) {
        new Swiper('[data-certificates]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 6,
            speed: 500,
            breakpoints: {
                1024: {
                    slidesPerView: 4,
                },
            },
        });
    }

    if (document.querySelector('[data-gallery]')) {

        const productThumbs = new Swiper('[data-thumbs]', {
            modules: [Thumbs, Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            watchSlidesProgress: true,
            slidesPerView: 3,
            spaceBetween: 4,
            direction: 'vertical',
            speed: 600,
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 6,
                    direction: 'horizontal',
                }
            },
        })



        const productGallery = new Swiper('[data-gallery]', {
            modules: [Thumbs, Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 600,
            navigation: {
                nextEl: '[data-gallery-next]',
                prevEl: '[data-gallery-prev]',
            },
            thumbs: {
                swiper: productThumbs,
            }
        })
    }

    if (document.querySelector('[data-tabs-header]')) {
        new Swiper('[data-tabs-header]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 4,
            speed: 500,
        });
    }

    if (document.querySelector('[data-filter-inline]')) {
        new Swiper('[data-filter-inline]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 4,
            speed: 600,
        });
    }

    if (document.querySelector('[data-compare-media]')) {
        new Swiper('[data-compare-media]', {
            modules: [Pagination],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 5,
            speed: 500,
            pagination: {
                el: '[data-compare-media-pagination]',
                clickable: true,
            },
            on: {}
        });
    }

    if (document.querySelector('[data-item-media]')) {
        new Swiper('[data-item-media]', {
            modules: [Pagination],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 10,
            speed: 500,
            pagination: {
                el: '[data-item-media-pagination]',
                clickable: true,
            },
            on: {}
        });
    }

}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
