export default () => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-copy]')) {
            console.log('copy')
            const self = event.target.closest('[data-copy]')
            const text = self.dataset.copy
            self.classList.add('copied')
            self.disabled = true
            navigator.clipboard.writeText(self.dataset.copy);

            console.log(self.dataset.copy)

            setTimeout(() => {
                self.classList.remove('copied')
                self.disabled = false
            }, 4000);
        }
    })
};
