
export default () => {
    document.addEventListener('click', (event) => {
        if (document.querySelectorAll('[data-phone]').length > 0) {
            const phones = document.querySelectorAll('[data-phone]');

            if (event.target.closest('[data-phone-button]')) {
                let phoneCode = event.target.closest('[data-phone-code]');
                if (phoneCode.classList.contains('open')) {
                    phoneCode.classList.remove('open');
                }
                else {
                    phoneCodeClose();
                    phoneCode.classList.add('open');
                }
            }
            else {
                if (!event.target.closest('[data-phone-dropdown]')) {
                    phoneCodeClose();
                }
            }

            if (event.target.closest('[data-phone-item]')) {
                event.target.closest('[data-phone]').querySelectorAll('[data-phone-item]').forEach(el => {
                    el.classList.remove('active');
                });
                event.target.closest('[data-phone-item]').classList.add('active')
                event.target.closest('[data-phone]').querySelector('[data-phone-flag]').innerHTML = event.target.closest('[data-phone-item]').querySelector('i').innerHTML
                phoneCodeClose();
            }

                function phoneCodeClose() {
                phones.forEach(el => {
                    el.querySelector('[data-phone-code]').classList.remove('open');
                });
            }
        }

    });
};
