export default () => {

    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-quantity-button]')) {
            const quantity = event.target.closest('[data-quantity]')
            const quantityInput = quantity.querySelector('[data-quantity-input]')
            const quantityMin = quantityInput.min
            const quantityMax = quantityInput.max
            const quantityDirection = event.target.closest('[data-quantity-button]').dataset.quantityButton
            let quantityValue = 0;

            if (!quantityInput.disabled) {
                if(quantityDirection === 'minus') {
                    quantityValue = parseInt(quantityInput.value) - 1
                    if (quantityValue < quantityMin) {
                        quantityValue = quantityMin
                    }
                }
                else {
                    quantityValue = parseInt(quantityInput.value) + 1
                    if (quantityValue > quantityMax) {
                        quantityValue = quantityMax
                    }
                }
                quantityInput.value = quantityValue
            }
        }
    })

    if (document.querySelectorAll('[data-quantity]').length > 0) {
        const quantityArray = Array.from(document.querySelectorAll('[data-quantity]'))

        quantityArray.forEach(elem => {
            elem.querySelector('[data-quantity-input]').addEventListener('change', (e) => {
                let quantity = elem.querySelector('[data-quantity-input]')

                let val = parseInt(quantity.value)

                if(!val) {
                    val = quantity.min
                }

                if (val < quantity.min) {
                    val = quantity.min
                }
                else if (val > quantity.max) {
                    val = quantity.max
                }

                quantity.value = val
            })
        });
    }
};
