"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
// import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import quantity from './forms/quantity.js' // input number
import fileField from './forms/file-field.js' // Select
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import Rating from './components/rating.js'; // Rating plugin
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import Tooltip from "./components/tooltip.js";
import customSelect from './components/customSelect.js' // Select
import inputPhone from './forms/input-phone.js' // Select
import Switcher from './components/switcher.js'; // switcher
import checkBoxed from './components/check-boxed.js'; // checkBoxed
import SiteSearch from './components/site-search.js'; // SiteSearch
import addCart from './components/add-cart.js'; // add Cart
import copyToClipboard from './components/copyToClipboard.js'; // Copy to clipboard
import headerFixed from './components/headerFixed.js'; // fixed header
import Choices from 'choices.js'; // Select plugin
import dynamicTextarea from "dynamic-textarea";

import IMask from 'imask';
import SimpleBar from 'simplebar'; // Кастомный скролл
import 'sticksy'
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
// maskInput('input[name="phone"]');

// input Number
quantity()

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Rating
Rating();

// Select Dropdown
customSelect()

fileField()

inputPhone()

// Tooltip
Tooltip()

copyToClipboard()

Switcher()

SiteSearch()

checkBoxed()

// Header fixed
headerFixed()

addCart() // Добавление в корзину

const tapBar = () => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-contacts-toggle]')) {
            console.log('data-contacts-toggle')
            document.querySelector('.tap-bar__item--contact').classList.toggle('active')
            document.querySelector('[data-contacts]').classList.toggle('open')
        }
    })
}


document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-toggle]')) {
        event.target.closest('[data-filter-toggle]').classList.toggle('open')
        event.target.closest('[data-filter]').classList.toggle('open')
    }
})

const range = () => {
    const rangeFieldsArray = document.querySelectorAll('.range-field__value')
    if (rangeFieldsArray.length > 0) {
        rangeFieldsArray.forEach(elem => {
            elem.onfocus = function () {
                elem.closest('.form-range').classList.add('focus')
                elem.closest('.range-field').classList.add('focus')
            }
            elem.onblur  = function () {
                elem.closest('.form-range').classList.remove('focus')
                elem.closest('.range-field').classList.remove('focus')

                if(!elem.value) {
                    elem.closest('.form-range').classList.remove('filled')
                    elem.closest('.range-field').classList.remove('filled')
                }
                else {
                    elem.closest('.form-range').classList.add('filled')
                    elem.closest('.range-field').classList.add('filled')
                }
            }
        })
    }
}

const select = () => {
    const selectsArray = document.querySelectorAll('.pretty-select')
    if (selectsArray.length > 0) {
        selectsArray.forEach(elem => {
            elem.onfocus = function () {
                elem.closest('.pretty-select').classList.add('filled')
            }
            elem.onblur  = function () {
                console.log(elem.options[elem.selectedIndex].text)
                if(elem.options[elem.selectedIndex].text === 'Не важно') {
                    elem.closest('.pretty-select').classList.remove('filled')
                }
                else {
                    elem.closest('.pretty-select').classList.add('filled')
                }
            }
        })
    }
}

// Compare

const compare = () => {

    if(document.querySelector('[data-compare-mobile]')) {
        window.addEventListener('scroll', function() {
            let label = document.querySelector('[data-compare-mobile]');
            if (document.documentElement.scrollTop > label.offsetTop) {
                document.querySelector('[data-compare-mobile-header]').classList.add('fixed');
                document.querySelector('[data-compare-mobile-main]').classList.add('fixed');
            } else {
                document.querySelector('[data-compare-mobile-header]').classList.remove('fixed');
                document.querySelector('[data-compare-mobile-main]').classList.remove('fixed');
            }
        });
    }

    if(document.querySelector('[data-compare]')) {
        const DIFFERENCES_SHOW =  document.querySelector('[data-differences-show]');
        const DIFFERENCES_ONLY =  document.querySelector('[data-differences-only]');
        window.addEventListener('scroll', function() {
            let compareLabel = document.querySelector('[data-compare-trigger]');
            console.log(compareLabel.getBoundingClientRect().top)
            if (compareLabel.getBoundingClientRect().top < 80) {
                document.body.classList.add('compare-fixed');
            } else {
                document.body.classList.remove('compare-fixed');
            }
        });

        DIFFERENCES_SHOW.addEventListener('change', (e) => {
            document.querySelectorAll('.compare__row').forEach(elem => {
                if (elem.classList.contains('compare-different')) {
                    elem.classList.toggle('active');
                }
            });
        })

        DIFFERENCES_ONLY.addEventListener('change', (e) => {
            if (DIFFERENCES_ONLY.checked) {
                DIFFERENCES_SHOW.checked = false
                DIFFERENCES_SHOW.setAttribute('disabled', '')
                document.querySelectorAll('.compare__row').forEach(elem => {
                    if (!elem.classList.contains('compare-different')) {
                        elem.classList.add('hide');
                    }
                    else {
                        elem.classList.remove('active');
                    }
                });
            }
            else {
                DIFFERENCES_SHOW.removeAttribute('disabled')
                document.querySelectorAll('.compare__row').forEach(elem => {
                    if (!elem.classList.contains('compare-different')) {
                        elem.classList.remove('hide');
                    }
                });
            }
        })
    }
}


// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
         //   autoHide: false
        });
    });
}

// if (document.querySelector('[data-scroll]')) {
//     const simpleBar = new SimpleBar(document.querySelector('[data-scroll]'));
//     console.log(simpleBar.getScrollElement())
// }
// Modal Fancybox
Fancybox.bind("[data-fancybox]", {
    autoFocus: false,
    Thumbs: false,
    // Thumbs: {
    //     type: "classic",
    // },
});

// Custom Select
document.querySelectorAll('.custom-select').forEach(el => {
    const select = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });

    select.passedElement.element.addEventListener(
        'change',
        function(event) {
            console.log(event.detail.value);
            if (event.detail.value === '0') {
                event.target.closest('.choices').classList.remove('filled')
            }
            else {
                event.target.closest('.choices').classList.add('filled')
            }
        },
        false,
    );
});


// Informers
document.addEventListener('click', (event) => {
    const informers = document.querySelectorAll('[data-informer]');
    if (informers.length > 0) {
        if (event.target.closest('[data-informer-toggle]')) {
            let informerContainer = event.target.closest('[data-informer]');
            if (informerContainer.classList.contains('open')) {
                informerContainer.classList.remove('open');
            }
            else {
                informerClose();
                informerContainer.classList.add('open');
            }
        }
        else {
            if (!event.target.closest('[data-informer-content]')) {
                informerClose();
            }
        }
        function informerClose() {
            informers.forEach(el => {
                el.classList.remove('open');
            });
        }
    }
});

/* Модуль работы с ползунком */
/*
Документация плагина: https://refreshless.com/nouislider/ */
import "./forms/range.js";

// Sliders
import "./components/sliders.js";



// Filter
document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-inline-item]')) {
        event.target.closest('[data-filter-inline]').querySelectorAll('[data-filter-inline-item]').forEach(elem => {
            elem.classList.remove('active')
        })
        event.target.closest('[data-filter-inline-item]').classList.add('active')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-inline-remove]')) {
        event.preventDefault()
        console.log('filter-inline-remove')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-showcase-sorter]')) {

        if (!event.target.closest('[data-showcase-sorter]').classList.contains('active')) {
            document.querySelector('.showcase-header').querySelectorAll('[data-showcase-sorter]').forEach(elem => {
                elem.classList.remove('active')
            })
            event.target.closest('[data-showcase-sorter]').classList.add('active')
        }

        event.target.closest('[data-showcase-sorter]').classList.toggle('toggle')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-sort-direction]')) {
        event.target.closest('[data-sort-direction]').classList.toggle('toggle')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-add-compare]')) {
        event.target.closest('[data-add-compare]').classList.toggle('added')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-add-favorite]')) {
        event.target.closest('[data-add-favorite]').classList.toggle('added')
    }
})

// Cart
const Purchase = () => {
    if (document.querySelectorAll('[data-purchase-item]').length > 0) {
        const PURCHASE = document.querySelector('[data-purchase]')
        const PURCHASE_AMOUNT = document.querySelector('[data-purchase-amount]')
        const PURCHASE_SUMMARY = document.querySelector('[data-purchase-summary]')
        const PURCHASE_DISCOUNT = document.querySelector('[data-purchase-discount]')
        const PURCHASE_TOTAL = document.querySelector('[data-purchase-total]')
        const PURCHASE_SUBMIT = document.querySelectorAll('[data-purchase-submit]')

        let purchaseItems = PURCHASE.querySelectorAll('[data-purchase-item]')
        let purchaseCheckArray = PURCHASE.querySelectorAll('[data-purchase-check]')

        purchaseCheckArray.forEach(el => {
            el.addEventListener('change', (event) => {
                setTimeout(function() {
                    checkedItems()
                }, 200)
            })
        })

        PURCHASE.querySelectorAll('[data-purchase-quantity]').forEach(el => {
            el.addEventListener('change', (event) => {
                setTimeout(function() {
                    checkedItems()
                }, 200)
            })
        })

        document.querySelector('[data-purchase-all]').addEventListener('change', (event) => {
            setTimeout(function() {
                checkedItems()
            }, 200)
        })

        document.addEventListener('click',  (event) => {
            if(event.target.closest('[data-quantity-button]')) {
                if(event.target.closest('[data-purchase-item]')) {
                    setTimeout(function() {
                        checkedItems()
                    }, 200)
                }
            }
        })

        function checkedItems() {
            let counter = 0
            let priceTotal = 0
            purchaseCheckArray.forEach((elem, i) => {
                if (elem.checked) {
                    let itemPrice = parseInt(elem.closest('[data-purchase-item]').querySelector('[data-purchase-price]').value)
                    let itemAmount = elem.closest('[data-purchase-item]').querySelector('[data-purchase-quantity]').value
                    counter = counter + 1 * itemAmount
                    priceTotal = priceTotal +  itemPrice * itemAmount
                }
            })
            let discount = parseInt(PURCHASE_DISCOUNT.dataset.purchaseDiscount)
            PURCHASE_AMOUNT.innerHTML = counter
            PURCHASE_SUMMARY.innerHTML = priceTotal.toLocaleString()
            PURCHASE_TOTAL.innerHTML = (priceTotal - parseInt(priceTotal / 100 * discount)).toLocaleString()
            if (priceTotal > 0) {
                PURCHASE_DISCOUNT.innerHTML = parseInt(priceTotal / 100 * discount).toLocaleString()
            }
            else {
                PURCHASE_DISCOUNT.innerHTML = 0
            }
            isPurchaseItems(counter)
        }

        function isPurchaseItems(count) {
            if (count > 0) {
                PURCHASE_SUBMIT.forEach(elem => {
                    elem.disabled = false
                })
            }
            else {
                PURCHASE_SUBMIT.forEach(elem => {
                    elem.disabled = true
                })
            }
        }
    }
}

Purchase()

const Popup = () => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-popup-open]')) {
            let popupTarget = event.target.closest('[data-popup-open]').dataset.popupOpen
            console.log(popupTarget)
            document.body.classList.add('popup-open')
            document.querySelector(`[data-popup="${popupTarget}"]`).classList.add('open');
        }

        if (event.target.closest('[data-popup-close]')) {
            document.body.classList.remove('popup-open')
            event.target.closest('[data-popup]').classList.remove('open')
        }
    })
}
Popup()

window.addEventListener("load", function (e) {

    if (document.querySelectorAll('[data-sticky-block]').length > 0) {

        const stickyEl = new Sticksy('[data-sticky-block]', {
            topSpacing: 108,
            listen: true
        })
    }

    document.querySelectorAll('[data-mask]').forEach(elem => {
        let maskPattern = elem.dataset.mask;
        IMask(elem, {
            mask: elem.dataset.mask
        });
    });

    tapBar()

    range()

    select()

    compare()


});
